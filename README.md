# babel-webpack-template

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

```bash
# Clone this repository
git clone https://gitlab.com/tebell/babel-webpack-template.git

# Go into the repository
cd babel-webpack-template

# Install dependencies
npm install

# Build and pack source
npm run build

# Run the app
npm start
```
